package it.webers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileExt extends java.io.File {

	public FileExt(String pathname) { super(pathname);}

	private static final long serialVersionUID = 1L;

	public String getContent() throws IOException {

		BufferedReader reader = new BufferedReader( new FileReader (this));
		String         line = null;
		StringBuilder  stringBuilder = new StringBuilder();
		String         ls = System.getProperty("line.separator");

		try {
			while( ( line = reader.readLine() ) != null ) {
			    stringBuilder.append( line );
			    stringBuilder.append( ls );
			}
		}catch(IOException e) { throw e; 
		}finally { try{ reader.close(); } catch(Exception e1) {} }

		return stringBuilder.toString();
	}
}
