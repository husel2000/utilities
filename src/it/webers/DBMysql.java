package it.webers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBMysql {
	private Connection connection;

	public DBMysql(String server, String port, String database, String user, String password) throws SQLException { connection = connect(server, port, database, user, password); } 
	
	private Connection connect(String server, String port, String database, String user, String password) throws SQLException{
		return DriverManager.getConnection("jdbc:mysql://" + server + ":" + port + "/" + database + "?" + "user=" + user + "&password=" + password);
	}

	/**
	 * Close active Connection. No committed changes are discarded.
	 */
	public void close() { try { connection.rollback(); } catch (Exception e) {} try { connection.close(); } catch (Exception e) {} }
	
	/**
	 * Executes a Select-Query.
	 * @param sql - Complete Select-Statement
	 * @return - ResultSet contains the Result of the Select-Statement
	 * @throws SQLException - Error during Select-Statement
	 */
	public ResultSet query(String sql) throws SQLException { return connection.createStatement().executeQuery(sql);	} 

	/**
	 * Execute a Update-Query
	 * @param sql - Complete Update-Statement
	 * @return - Numbers of Rows Changed (if not supported - 0)
	 * @throws SQLException - Error during Update.Query
	 */
	public int update(String sql) throws SQLException { return connection.createStatement().executeUpdate(sql); }
	
	/**
	 * Gets a PreparedStatement for Select / Update / Delete Data from Database
	 * @param sql - Complete Statement for PreparedStatement
	 * @return	- PreparedStatement
	 * @throws SQLException - Error during PrepareStatement
	 */
	public PreparedStatement getPreparedStatment(String sql) throws SQLException { return connection.prepareStatement(sql); }
}
