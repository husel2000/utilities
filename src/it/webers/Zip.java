package it.webers;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zip extends ZipOutputStream {

	public Zip(OutputStream out) { super(out); }
	
	public void addToZip(File file) throws IOException { addToZip(file,null); }
	
	public void addToZip(File file, FileFilter filter) throws IOException {
		
		//if(!file.canRead())throw new FileNotFoundException("File/Filder " + file.getAbsolutePath() + " can not read");
		
		if(file.isDirectory()) {
			File[] files;
			if(filter != null) files = file.listFiles(filter);
			else files = file.listFiles();
			
			for(int j = 0; j < files.length; j++) addToZip(files[j]);
		}else if(file.isFile()) {			
	        putNextEntry(new ZipEntry(file.getAbsolutePath()));
	        FileInputStream fis = new FileInputStream(file);
	        
	        byte[] buffer = new byte[4092]; int byteCount = 0;
	        while ((byteCount = fis.read(buffer)) != -1) write(buffer, 0, byteCount);
	
	        fis.close();
	        closeEntry();
		}
	}

}
