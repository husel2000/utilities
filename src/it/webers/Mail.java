package it.webers;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Mail {
	public static boolean isValid(String email) {
		try {
			new InternetAddress(email).validate();
			return true;
		} catch (AddressException ex) { return false; }
	}
}
